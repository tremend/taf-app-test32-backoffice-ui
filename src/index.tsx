import React from 'react'
import ReactDOM from 'react-dom'
// Redux
// https://github.com/rt2zz/redux-persist
import {Provider} from 'react-redux'
import AppComponent from './app';
// Axios

// Apps
import {App} from './app/App'
import {MetronicI18nProvider} from './_metronic/i18n/Metronici18n'
import './_metronic/assets/sass/style.scss'
import getStore from './app/config/store'
import {registerLocale} from './app/config/translation'
import {clearAuthentication} from './app/shared/reducers/authentication'
import {bindActionCreators} from 'redux'
import setupAxiosInterceptors from './app/config/axios-interceptor'
import {loadIcons} from './app/config/icon-loader'
import ErrorBoundary from 'app/shared/error/error-boundary'
const {PUBLIC_URL} = process.env

// /* const mock = */ _redux.mockAxios(axios)
// _redux.setupAxios(axios, store)
//
// Chart.register(...registerables)

const store = getStore();
registerLocale(store);

const actions = bindActionCreators({ clearAuthentication }, store.dispatch);
setupAxiosInterceptors(() => actions.clearAuthentication('login.error.unauthorized'));

loadIcons();

const rootEl = document.getElementById('root');

// ReactDOM.render(
//   // <MetronicI18nProvider>
//     <Provider store={store}>
//       {/* Asynchronously persist redux stores and show `SplashScreen` while it's loading. */}
//       {/*<PersistGate persistor={persistor} loading={<div>Loading...</div>}>*/}
//         <App basename={PUBLIC_URL} />
//       {/*</PersistGate>*/}
//     </Provider>,
//   // </MetronicI18nProvider>,
//   rootEl
// )

const render = Component =>
  // eslint-disable-next-line react/no-render-return-value
  ReactDOM.render(
    <ErrorBoundary>
      <Provider store={store}>
        <Component />
      </Provider>
    </ErrorBoundary>,
    rootEl
  );

render(AppComponent);
