/* eslint-disable jsx-a11y/anchor-is-valid */
import {FC} from 'react'
import {shallowEqual, useSelector} from 'react-redux'
import {Link} from 'react-router-dom'
import {UserModel} from '../../../../app/modules/auth/models/UserModel'
import {RootState} from '../../../../setup'
import {Languages} from './Languages'
import {useAppSelector} from '../../../../app/config/store'

const HeaderUserMenu: FC = () => {
  const account = useAppSelector(state => state.authentication.account);

  return (
    <div
      className='menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px'
      data-kt-menu='true'
    >
      <div className='menu-item px-3'>
        <div className='menu-content d-flex align-items-center px-3'>
          <div className='symbol symbol-50px me-5'>
            {/*<img alt='Logo' src={user?.pic} />*/}
          </div>

          <div className='d-flex flex-column'>
            <div className='fw-bolder d-flex align-items-center fs-5'>
              {account.login}
            </div>
            <a href='#' className='fw-bold text-muted text-hover-primary fs-7'>
              {account?.email}
            </a>
          </div>
        </div>
      </div>

      {/*<Languages />*/}

      <div className='menu-item px-5'>
        <Link to='/account/settings' className='menu-link px-5'>
          Settings
        </Link>
      </div>

      <div className='menu-item px-5'>
        <Link to='/account/password' className='menu-link px-5'>
          Password
        </Link>
      </div>

      <div className='menu-item px-5'>
        <Link to='/logout' className='menu-link px-5'>
          Sign Out
        </Link>
      </div>
    </div>
  )
}

export {HeaderUserMenu}
