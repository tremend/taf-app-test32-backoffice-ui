/* eslint-disable react/jsx-no-target-blank */
import React from 'react'
import {KTSVG} from '../../../helpers'
import {AsideMenuItemWithSub} from './AsideMenuItemWithSub'
import {AsideMenuItem} from './AsideMenuItem'
import {EntitiesMenu} from '../../../../app/shared/layout/menus'
import {useAppSelector} from '../../../../app/config/store'
import {hasAnyAuthority} from '../../../../app/shared/auth/private-route'
import {AUTHORITIES} from '../../../../app/config/constants'

export function AsideMenuMain() {

  const currentLocale = useAppSelector(state => state.locale.currentLocale);
  const isAuthenticated = useAppSelector(state => state.authentication.isAuthenticated);
  const isAdmin = useAppSelector(state => hasAnyAuthority(state.authentication.account.authorities, [AUTHORITIES.ADMIN]));
  const ribbonEnv = useAppSelector(state => state.applicationProfile.ribbonEnv);
  const isInProduction = useAppSelector(state => state.applicationProfile.inProduction);
  const isOpenAPIEnabled = useAppSelector(state => state.applicationProfile.isOpenAPIEnabled);

  return (
    <>
      <AsideMenuItem
        to='/'
        icon='/media/icons/duotone/Design/Layers.svg'
        title='Dashboard'
        fontIcon='bi-layers'
      />
      <div className='menu-item'>
        <div className='menu-content pt-8 pb-2'>
          <span className='menu-section text-muted text-uppercase fs-8 ls-1'>ENTITIES</span>
        </div>
      </div>
      <EntitiesMenu />
      { isAdmin && <>
        <div className='menu-item'>
          <div className='menu-content pt-8 pb-2'>
            <span className='menu-section text-muted text-uppercase fs-8 ls-1'>Administration</span>
          </div>
        </div>
        <AsideMenuItem
        to='/admin/user-management'
        icon='/media/icons/duotone/General/User.svg'
        title='User management'
        fontIcon='bi-layers'
        />
        <AsideMenuItem
          to='/admin/metrics'
          icon='/media/icons/duotone/Shopping/Chart-line1.svg'
          title='Metrics'
          fontIcon='bi-layers'
        />
        <AsideMenuItem
          to='/admin/health'
          icon='/media/icons/duotone/General/Heart.svg'
          title='Health'
          fontIcon='bi-layers'
        />
        <AsideMenuItem
          to='/admin/configuration'
          icon='/media/icons/duotone/Interface/Settings-02.svg'
          title='Configuration'
          fontIcon='bi-layers'
        />
        <AsideMenuItem
          to='/admin/logs'
          icon='/media/icons/duotone/Text/Toggle.svg'
          title='Logs'
          fontIcon='bi-layers'
        />
        {
          isOpenAPIEnabled &&
          <AsideMenuItem
            to='/admin/docs'
            icon='/media/icons/duotone/Electric/Outlet.svg'
            title='API'
            fontIcon='bi-layers'
          />
        }
      </>
      }
    </>
  )
}
