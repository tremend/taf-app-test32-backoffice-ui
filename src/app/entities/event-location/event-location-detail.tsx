import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getEntity } from './event-location.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const EventLocationDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const eventLocationEntity = useAppSelector(state => state.eventLocation.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="eventLocationDetailsHeading">
          <Translate contentKey="test32App.eventLocation.detail.title">EventLocation</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{eventLocationEntity.id}</dd>
          <dt>
            <span id="name">
              <Translate contentKey="test32App.eventLocation.name">Name</Translate>
            </span>
          </dt>
          <dd>{eventLocationEntity.name}</dd>
          <dt>
            <span id="maxCapacity">
              <Translate contentKey="test32App.eventLocation.maxCapacity">Max Capacity</Translate>
            </span>
          </dt>
          <dd>{eventLocationEntity.maxCapacity}</dd>
          <dt>
            <span id="details">
              <Translate contentKey="test32App.eventLocation.details">Details</Translate>
            </span>
          </dt>
          <dd>{eventLocationEntity.details}</dd>
          <dt>
            <span id="city">
              <Translate contentKey="test32App.eventLocation.city">City</Translate>
            </span>
          </dt>
          <dd>{eventLocationEntity.city}</dd>
          <dt>
            <span id="country">
              <Translate contentKey="test32App.eventLocation.country">Country</Translate>
            </span>
          </dt>
          <dd>{eventLocationEntity.country}</dd>
          <dt>
            <span id="lng">
              <Translate contentKey="test32App.eventLocation.lng">Lng</Translate>
            </span>
          </dt>
          <dd>{eventLocationEntity.lng}</dd>
          <dt>
            <span id="lat">
              <Translate contentKey="test32App.eventLocation.lat">Lat</Translate>
            </span>
          </dt>
          <dd>{eventLocationEntity.lat}</dd>
        </dl>
        <Button tag={Link} to="/event-location" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/event-location/${eventLocationEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default EventLocationDetail;
