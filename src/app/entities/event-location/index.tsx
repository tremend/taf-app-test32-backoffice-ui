import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import EventLocation from './event-location';
import EventLocationDetail from './event-location-detail';
import EventLocationUpdate from './event-location-update';
import EventLocationDeleteDialog from './event-location-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={EventLocationUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={EventLocationUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={EventLocationDetail} />
      <ErrorBoundaryRoute path={match.url} component={EventLocation} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={EventLocationDeleteDialog} />
  </>
);

export default Routes;
