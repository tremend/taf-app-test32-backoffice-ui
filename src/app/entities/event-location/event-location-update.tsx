import React, { useState, useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, Translate, translate, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getEntity, updateEntity, createEntity, reset } from './event-location.reducer';
import { IEventLocation } from 'app/shared/model/event-location.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const EventLocationUpdate = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const eventLocationEntity = useAppSelector(state => state.eventLocation.entity);
  const loading = useAppSelector(state => state.eventLocation.loading);
  const updating = useAppSelector(state => state.eventLocation.updating);
  const updateSuccess = useAppSelector(state => state.eventLocation.updateSuccess);

  const handleClose = () => {
    props.history.push('/event-location' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(props.match.params.id));
    }
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    const entity = {
      ...eventLocationEntity,
      ...values,
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {}
      : {
          ...eventLocationEntity,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="test32App.eventLocation.home.createOrEditLabel" data-cy="EventLocationCreateUpdateHeading">
            <Translate contentKey="test32App.eventLocation.home.createOrEditLabel">Create or edit a EventLocation</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? (
                <ValidatedField
                  name="id"
                  required
                  readOnly
                  id="event-location-id"
                  label={translate('global.field.id')}
                  validate={{ required: true }}
                />
              ) : null}
              <ValidatedField
                label={translate('test32App.eventLocation.name')}
                id="event-location-name"
                name="name"
                data-cy="name"
                type="text"
              />
              <ValidatedField
                label={translate('test32App.eventLocation.maxCapacity')}
                id="event-location-maxCapacity"
                name="maxCapacity"
                data-cy="maxCapacity"
                type="text"
              />
              <ValidatedField
                label={translate('test32App.eventLocation.details')}
                id="event-location-details"
                name="details"
                data-cy="details"
                type="text"
              />
              <ValidatedField
                label={translate('test32App.eventLocation.city')}
                id="event-location-city"
                name="city"
                data-cy="city"
                type="text"
              />
              <ValidatedField
                label={translate('test32App.eventLocation.country')}
                id="event-location-country"
                name="country"
                data-cy="country"
                type="text"
              />
              <ValidatedField
                label={translate('test32App.eventLocation.lng')}
                id="event-location-lng"
                name="lng"
                data-cy="lng"
                type="text"
              />
              <ValidatedField
                label={translate('test32App.eventLocation.lat')}
                id="event-location-lat"
                name="lat"
                data-cy="lat"
                type="text"
              />
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/event-location" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default EventLocationUpdate;
