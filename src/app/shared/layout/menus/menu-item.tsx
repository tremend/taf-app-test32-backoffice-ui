import React from 'react';
import { DropdownItem } from 'reactstrap';
import { NavLink as Link, useLocation } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import clsx from 'clsx';
import {checkIsActive, KTSVG } from '_metronic/helpers';
import { useLayout } from '_metronic/layout/core';

export interface IMenuItem {
  icon: IconProp;
  to: string;
  id?: string;
  'data-cy'?: string;
}

const MenuItem = (props) =>  {
  const { to, icon, id, children } = props;

  const {pathname} = useLocation()
  const isActive = checkIsActive(pathname, to)
  const {config} = useLayout()
  const {aside} = config

  return (
    <div className='menu-item'>
      <Link className={clsx('menu-link without-sub', {active: isActive})} to={to}>
        {icon && aside.menuIcon === 'svg' && (
          <span className='menu-icon'>
          <KTSVG path='/media/icons/duotone/Files/Folder.svg' className='svg-icon-2' />
        </span>
        )}
        {/*{fontIcon && aside.menuIcon === 'font' && <i className={clsx('bi fs-3', fontIcon)}></i>}*/}
        <span className='menu-title'>{children}</span>
      </Link>
    </div>)
}

export default MenuItem;
