import { IProductCategory } from 'app/shared/model/product-category.model';

export interface IProduct {
  id?: number;
  name?: string | null;
  description?: string | null;
  price?: number | null;
  imageURL?: string | null;
  category?: IProductCategory | null;
}

export const defaultValue: Readonly<IProduct> = {};
