export interface IProductCategory {
  id?: number;
  name?: string | null;
  description?: string | null;
}

export const defaultValue: Readonly<IProductCategory> = {};
