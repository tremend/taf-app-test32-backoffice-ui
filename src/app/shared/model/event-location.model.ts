import { IEvent } from 'app/shared/model/event.model';

export interface IEventLocation {
  id?: number;
  name?: string | null;
  maxCapacity?: number | null;
  details?: string | null;
  city?: string | null;
  country?: string | null;
  lng?: string | null;
  lat?: string | null;
  events?: IEvent[] | null;
}

export const defaultValue: Readonly<IEventLocation> = {};
