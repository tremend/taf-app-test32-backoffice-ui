import dayjs from 'dayjs';
import { IEventLocation } from 'app/shared/model/event-location.model';

export interface IEvent {
  id?: number;
  name?: string | null;
  description?: string | null;
  image?: string | null;
  startDate?: string | null;
  endDate?: string | null;
  location?: IEventLocation | null;
}

export const defaultValue: Readonly<IEvent> = {};
