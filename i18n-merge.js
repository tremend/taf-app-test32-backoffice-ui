const {readdirSync, writeFileSync, readFileSync} = require("fs")

const getFiles = source =>
    readdirSync(source, {withFileTypes: true})
        .map(dirent => `${source}/${dirent.name}`)

const getDirectories = source =>
    readdirSync(source, {withFileTypes: true})
        .filter(dirent => dirent.isDirectory())
        .map(dirent => dirent.name)

const dirs = getDirectories("./src/i18n");

dirs.forEach(dir => {
    const files = getFiles(`./src/i18n/${dir}`);
    const result =  files.reduce((or, val) => {
        const obj = JSON.parse(readFileSync(val, 'utf8'));
        return mergeDeep(or, obj)
    }, {})

    writeFileSync(`./public/i18n/${dir}.json`, JSON.stringify(result))
})

function isObject(item) {
    return (item && typeof item === 'object' && !Array.isArray(item));
}

function mergeDeep(target, ...sources) {
    if (!sources.length) return target;
    const source = sources.shift();

    if (isObject(target) && isObject(source)) {
        for (const key in source) {
            if (isObject(source[key])) {
                if (!target[key]) Object.assign(target, { [key]: {} });
                mergeDeep(target[key], source[key]);
            } else {
                Object.assign(target, { [key]: source[key] });
            }
        }
    }

    return mergeDeep(target, ...sources);
}
