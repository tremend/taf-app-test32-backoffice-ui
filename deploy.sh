#!/bin/bash

set -e

sed 's/<build_number>/'"$1"'/g' kubernetes.yml > kubernetes-production.yml

cat kubernetes-production.yml
kubectl apply --kubeconfig ~/.kube/internal -f kubernetes-production.yml
