FROM node:16 as builder

WORKDIR /app
ADD . .
RUN npm install --force
RUN npm run build

FROM nginx:1.15-alpine

WORKDIR /app
COPY --from=builder /app/build/ /usr/share/nginx/html
ADD site.conf /etc/nginx/conf.d/default.conf
